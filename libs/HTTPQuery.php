<?php

/**
 * @name: HTTPQuery.php
 * @description: Thing to work with HTTP protocol
 * @requires: WhoIS class by NULL_byte >= ver 1.1
 * @author: NULL_byte
 * @contacts: www.null-byte.info
 * @version: 2.1
 */

# http://ru.php.net/manual/en/ref.url.php
# TODO: refactor HTTPSocket class using <http://ru.php.net/manual/en/ref.http.php>
#  & discover <http://ru.php.net/manual/en/class.httpresponse.php>

# The better HTTP Client interface will always work via HTTPQuery class
# usage example:
#   $q = new HTTPQuery;
#   $q->Get('http://www.yandex.ru/');


# Import WhoIS class
if (!class_exists('WhoIS'))
	@require_once dirname(__FILE__).'/WhoIS.php';
if (!class_exists('WhoIS'))
	die("The required class `WhoIS` is not found!");

abstract class HTTPClient extends WhoIS {
	# System operated variables
	public $URI          = 'http://localhost/';
	public $Scheme       = 'http';
	public $Host         = null;
	public $IDNA         = null; # Punycode
	public $Port         = '80';
	public $Path         = null;
	public $Method       = 'get';
	public $Query        = null;
	public $Cookies      = null;
	public $Engine       = null; # 'cURL' or 'Sockets'
	# Toggles
	public $Proxy        = false; # Proxy to use. Format: Array('proxy_host', 'port') or false or 'proxy:port', yeah
	#public $UserAgent    = 'Opera/8.01 (J2ME/MIDP; Opera Mini/2.0.4719; en; U; ssr)';
	public $UserAgent    = false;
	public $Referer      = null;
	public $AutoCookies  = true;
	public $AutoReferer  = true;
	public $AutoRedirect = true; # TODO: fix header('Location: /redirect.html');   # Fixed?
	public $FlushCookies = false; # WTF?!  will flush cookies after each request if $this->AutoCookies = false;
	public $Verbose      = false; # If true, would print many interesting messages
	public $Timeout      = 30; # Request timeout
	# Output
	public $Result       = null;  # Will contain result with page headers
	public $ResultClean  = null;  # Will contain result without page headers
	# Headers
	public $HTTPHeaders  = Array(); # Example:  $this->HTTPHeaders = Array('Location: /newpage.html', ...);
	public $PageHeaders  = Array(); # Example:  $this->PageHeaders['location'];
	# Aliases
	public $ResponseHeaders = Array(); # alias of $this->PageHeaders
	public $Headers         = Array(); # alias of $this->PageHeaders
	public $RequestHeaders  = Array(); # alias of $this->HTTPHeaders
	public $RawHeaders      = Array(); # alias of $this->HTTPHeaders

	public $maxRedirects    = 20;
	public $maxRedirects_i  = 0;

	public $Redirect_i      = 0; # should be reset manualy... (updates only on request!)

	#private $gotCookies_    = null;
	public $gotCookies_    = null;
	public $gotCookies__    = null;

	abstract public function Get($url);
	abstract public function Post($url);
	#abstract public function GetCookies();
	abstract protected function SendRequest();
	abstract protected function ConstructQuery();

	public function __construct ($dump_engine = false) {
		$this->Engine = get_parent_class($this);
		#if (strtolower(substr($this->Engine, 0, 4)) == 'http')
		#	$this->Engine = substr($this->Engine, 4);
		if ($dump_engine)
			echo " # `".get_class($this)."` currently uses `<{$this->Engine}>` as HTTP engine\r\n";
	}
	
	public function rndUserAgent() { # needs to be improved
		if(mt_rand(0,1)) $this->UserAgent = 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)';
		if(mt_rand(0,1)) $this->UserAgent = 'Mozilla/5.0 (compatible; Opera 9.80; Windows NT 6)';
		if(mt_rand(0,1)) $this->UserAgent = 'Safari Mozilla 4.6';
		if(mt_rand(0,1)) $this->UserAgent = 'Mozilla/3.0 Googlebot';
		if(mt_rand(0,1)) $this->UserAgent = 'Mozilla/5.0 (X11; U; Linux x86_64;) Gecko Ubuntu/9.04 (jaunty) Shiretoko/3.5';
		if(mt_rand(0,1)) $this->UserAgent = 'Mozilla/4.61 [en] (OS/2; U)';
		if(mt_rand(0,1)) $this->UserAgent = 'Mozilla/4.8 [en] (Windows NT 5.0; U)';
		if(mt_rand(0,1)) $this->UserAgent = 'Opera/8.0 (X11; Linux i686; U; cs)';
		if(mt_rand(0,1)) $this->UserAgent = 'Opera/10.00 (Windows NT 6.0; U; en) Presto/2.2.0';
		#if(mt_rand(0,1)) $this->UserAgent = 'Opera/8.01 (J2ME/MIDP; Opera Mini/2.0.4719; en; U; ssr)';
	}

	protected function MakeAliases() {
		if (!$this->PageHeaders && $this->ResponseHeaders) $this->PageHeaders = $this->ResponseHeaders;
		if (!$this->PageHeaders && $this->Headers)         $this->PageHeaders = $this->Headers;
		if (!$this->HTTPHeaders && $this->RequestHeaders)  $this->HTTPHeaders = $this->RequestHeaders;
		if (!$this->HTTPHeaders && $this->RawHeaders)      $this->HTTPHeaders = $this->RawHeaders;
	}

	protected function PreConstructQuery() {
		$this->getUrlParts();
		list($this->IDNA, $this->Host) = $this->IDNA($this->Host);
		if ($this->IDNA) $this->Host = $this->IDNA;
	}

	protected function getUrlParts($url = false) {
		if (!$url && !$this->URI) return false;
		if (!$url) $url = $this->URI;

		$url_parts = parse_url($url);
		if (!isset($url_parts['scheme'])) $url_parts['scheme'] = 'http';
		if (!isset($url_parts['port']))
			$url_parts['port'] = ($url_parts['scheme'] == 'https') ? 443 : 80;
		if (!isset($url_parts['path'])) $url_parts['path'] = null;
		if (!isset($url_parts['query']))
			$url_parts['query'] = null;
		else
			$url_parts['query'] = "?{$url_parts['query']}";

		$this->Scheme = strtolower($url_parts['scheme']);
		$this->Host = strtolower($url_parts['host']);
		$this->Port = $url_parts['port'];
		$this->Path = $url_parts['path'];
		$this->Query = $url_parts['query'];

		return $url_parts;
	}

	protected function ParseHeaders() {
		if (!$this->Result) return false;
		$server_response = explode("\r\n\r\n", $this->Result);
		$server_response = $server_response[0];
		$server_response = str_replace("\r", '', $server_response);
		$server_response = explode("\n", $server_response);

		$this->ResponseStatus = $server_response[0];
		preg_match('#\d{3}#', $this->ResponseStatus, $this->ResponseCode);
		if (isset($this->ResponseCode[0]))
			$this->ResponseCode = $this->ResponseCode[0];

		$result = Array();
		foreach($server_response as $mixed_headers) {
			if ((strpos($mixed_headers, ': ') >= 0)) {
				$header = explode(': ', trim($mixed_headers));
				$result[$header[0]] = isset($header[1]) ? $header[1] : NULL;
				$result[strtolower($header[0])] = isset($header[1]) ? $header[1] : NULL;
			}
		}
		$this->ResponseHeaders = $result;
		$this->PageHeaders = $result;
		$this->Headers = $result;
		return $result;
	}

	protected function CookiesToString($cookie = false) {
		if (!$cookie) $cookie = $this->Cookies;
		if (!$cookie) return $cookie;
		if (is_string($cookie)) return $cookie;
		#if (!is_array($cookie)) var_dump($cookie);
		
		$cookie_string = '';
		foreach ($cookie as $key => $value)
			$cookie_string .= " {$key}={$value};";
		$cookie = trim($cookie_string, '; ');
		return $cookie;
	}

	protected function CookiesToArray($cookie = false) {
		if (!$cookie) $cookie = $this->Cookies;
		if (is_array($cookie)) return $cookie;
		$cookie = explode('; ', $cookie);
		$cookie_array = Array();
		for ($i = 0; $i < count($cookie); $i++) {
			$c = explode('=', $cookie[$i]);
			$cookie_array[$c[0]] = $c[1];
		}
		return $cookie_array;
	}

	public function GetCookies($as_string = false) {
		if (!$this->Result && $as_string) return null;
		if (!$this->Result) return Array();
		$data = explode("\r\n\r\n", $this->Result);
		$data = $data[0];
		$data = str_replace("\r", '', $data);
		$data = explode("\n", $data);
		$raw_cookies = Array();
		foreach ($data as $d) {
			$d = explode(' ', $d);
			if (strtolower($d[0]) == 'set-cookie:') {
				$raw_cookies[] = $d[1];
			}
		}
		$cookies = Array();
		foreach ($raw_cookies as $cookie) {
			$cookie_name = substr($cookie, 0, strpos($cookie, '='));
			$cookie_value = substr($cookie, strpos($cookie, '=')+1, strpos($cookie, ';')-2);
			$cookies[$cookie_name] = trim($cookie_value, '; ');
		}
		if ($as_string)
			$cookies = $this->CookiesToString($cookies);
		#$this->Cookies = $cookies;
		return $cookies;
	}

	public function ClearCookies() {
		$this->Cookies = null;
	}


	# Pa-paa-ram!
	# Checks if string is UTF-8 encoded
	function is_utf8($string, $utf8_split = 5000) { // v1.01
		if (strlen($string) > $utf8_split) {
			for ($i=0,$s=$utf8_split,$j=ceil(strlen($string)/$utf8_split);$i < $j;$i++,$s+=$utf8_split) {
				if ($this->is_utf8(substr($string,$s,$utf8_split)))
					return true;
			}
			return false;
		} else {
			return preg_match('%^(?:
				[\x09\x0A\x0D\x20-\x7E]              # ASCII
				| [\xC2-\xDF][\x80-\xBF]             # non-overlong 2-byte
				|  \xE0[\xA0-\xBF][\x80-\xBF]        # excluding overlongs
				| [\xE1-\xEC\xEE\xEF][\x80-\xBF]{2}  # straight 3-byte
				|  \xED[\x80-\x9F][\x80-\xBF]        # excluding surrogates
				|  \xF0[\x90-\xBF][\x80-\xBF]{2}     # planes 1-3
				| [\xF1-\xF3][\x80-\xBF]{3}          # planes 4-15
				|  \xF4[\x80-\x8F][\x80-\xBF]{2}     # plane 16
			)*$%xs', $string);
		}
	}
}


# If cURL  extension is available, we will use it...
# else we'll be lived without HTTP/1.1 & HTTPS support

#####     #####     #####     #####     #####     #####     #####     #####     #####     #####     #####     #####     #####
#####     #####     #####     #####     #####     #####     #####     #####     #####     #####     #####     #####     #####

#####     #####     #####     #####     #####     #####

#####     Working with HTTP via cURL...           #####

#####     #####     #####     #####     #####     #####

#####     #####     #####     #####     #####     #####     #####     #####     #####     #####     #####     #####     #####
#####     #####     #####     #####     #####     #####     #####     #####     #####     #####     #####     #####     #####


# cURL based interface
class HTTPcURL extends HTTPClient {
	var $cURLHandler = null;

	function ConstructQuery() {
		if ($this->Verbose) {
			file_put_contents("./_request_#{$this->Redirect_i}.log", null);
			echo  " # Client request log created (`./_request_#{$this->Redirect_i}.log`)\n";
		}
		$this->MakeAliases();

		$this->cURLHandler = curl_init();
		curl_setopt($this->cURLHandler, CURLOPT_FRESH_CONNECT, true);
		curl_setopt($this->cURLHandler, CURLOPT_FORBID_REUSE, true);
		curl_setopt($this->cURLHandler, CURLOPT_USERAGENT, $this->UserAgent);
		if ($this->Verbose) {
			file_put_contents("./_request_#{$this->Redirect_i}.log", 
				"UserAgent: `".print_r($this->UserAgent, true)."`\r\n", FILE_APPEND);
			echo  " # Client request log [`./_request_#{$this->Redirect_i}.log`] updated with `UserAgent`\r\n";
		}
		curl_setopt($this->cURLHandler, CURLOPT_TIMEOUT, $this->Timeout);
		curl_setopt($this->cURLHandler, CURLOPT_HEADER, true);
		#curl_setopt($this->cURLHandler, CURLINFO_HEADER_OUT, true);
		curl_setopt($this->cURLHandler, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($this->cURLHandler, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($this->cURLHandler, CURLOPT_AUTOREFERER, $this->AutoReferer);
		#curl_setopt($this->cURLHandler, CURLOPT_COOKIESESSION, true);
		curl_setopt($this->cURLHandler, CURLOPT_FOLLOWLOCATION, false);
		curl_setopt($this->cURLHandler, CURLOPT_VERBOSE, $this->Verbose);
		curl_setopt($this->cURLHandler, CURLOPT_NOPROGRESS, !$this->Verbose);
		curl_setopt($this->cURLHandler, CURLOPT_FAILONERROR, false);
		if ($this->Proxy) {
			if (is_string($this->Proxy))
				$this->Proxy = explode(':', $this->Proxy);
			curl_setopt($this->cURLHandler, CURLOPT_PROXY, $this->Proxy[0]);
			curl_setopt($this->cURLHandler, CURLOPT_PROXYPORT, $this->Proxy[1]);
			curl_setopt($this->cURLHandler, CURLOPT_HTTPPROXYTUNNEL, true);
			if ($this->Verbose) {
				file_put_contents("./_request_#{$this->Redirect_i}.log", 
					"Proxy: `".trim(print_r($this->Proxy, true))."`\r\n", FILE_APPEND);
				echo  " # Client request log [`./_request_#{$this->Redirect_i}.log`] updated with `Proxy`\r\n";
			}
		} else {
			curl_setopt($this->cURLHandler, CURLOPT_HTTPPROXYTUNNEL, false);
		}
		
		

		# CURLOPT_COOKIE


		if ($this->AutoReferer && !$this->Referer && $this->Host)
			$this->Referer = "{$this->Scheme}://{$this->Host}/";


		if ($this->Referer) {
			curl_setopt($this->cURLHandler, CURLOPT_REFERER, $this->Referer);
			if ($this->Verbose) {
				file_put_contents("./_request_#{$this->Redirect_i}.log", 
					"Referer: `".trim(print_r($this->Referer, true))."`\r\n", FILE_APPEND);
				echo  " # Client request log [`./_request_#{$this->Redirect_i}.log`] updated with `Referer`\r\n";
			}
		}

		#if ($this->AutoCookies && $this->Cookies) {
		if ($this->Cookies) {
			curl_setopt($this->cURLHandler, CURLOPT_COOKIE, $this->CookiesToString());
			if ($this->Verbose) {
				file_put_contents("./_request_#{$this->Redirect_i}.log", 
					"Cookies: `".$this->CookiesToString()."`\r\n", FILE_APPEND);
				echo  " # Client request log [`./_request_#{$this->Redirect_i}.log`] updated with `Cookies`\r\n";
			}
		}

		if ($this->HTTPHeaders) {
			curl_setopt($this->cURLHandler, CURLOPT_HTTPHEADER, $this->HTTPHeaders);
			if ($this->Verbose) {
				file_put_contents("./_request_#{$this->Redirect_i}.log", 
					"HTTP Headers: `".trim(print_r($this->HTTPHeaders, true))."`\r\n", FILE_APPEND);
				echo  " # Client request log [`./_request_#{$this->Redirect_i}.log`] updated with `HTTPHeaders`\r\n";
			}
		}
		#curl_setopt($this->cURLHandler, CURLOPT_POST, true);
		#curl_setopt($this->cURLHandler, CURLOPT_HTTPGET, false);
		#curl_setopt($this->cURLHandler, CURLOPT_POSTFIELDS, $this->Query);

		if ($this->Verbose) {
			file_put_contents("./_request_#{$this->Redirect_i}.log", "\r\n", FILE_APPEND);
				echo  " # Client request log [`./_request_#{$this->Redirect_i}.log`] updated with `new line (".'\r\n'.")`\r\n";
		}
		return $this->cURLHandler;
	}


	function SendRequest($url = false, $ch = false) {
		if (!$url && !$this->URI) return false;
		if (!$url) $url = $this->URI;
		else $this->URI = $url;

		$this->PreConstructQuery();
		if (!$ch) $this->cURLHandler = $this->ConstructQuery();
		else $this->cURLHandler = $ch;
		
		#var_dump("{$this->Scheme}://{$this->Host}{$this->Path}{$this->Query}");
		#var_dump($this);

		curl_setopt($this->cURLHandler, CURLOPT_URL,
			"{$this->Scheme}://{$this->Host}{$this->Path}{$this->Query}");

		curl_setopt($this->cURLHandler, CURLOPT_PORT, $this->Port);
		curl_setopt($this->cURLHandler, CURLOPT_RETURNTRANSFER, true);

		$this->Result = curl_exec($this->cURLHandler);
		curl_close($this->cURLHandler);

		# clean 100 continue waste
		while (preg_match('#^http/1\.[01] [^\r\n]+\r?\n\r?\n#si', $this->Result))
			$this->Result = preg_replace("#^http/1\.[01] [^\r\n]+\r?\n\r?\n#si", '', $this->Result);
		#if (preg_match('#^http/1\.[01] [^\r\n]+\r\n\r\n#i', $this->Result))
		#	$this->Result = preg_replace("#^http/1\.[01] [^\r\n]+\r\n\r\n#i", '', $this->Result);


		if ($this->Verbose) {
			file_put_contents("./_responce_#{$this->Redirect_i}.log", null);
			echo  " # Server responce log created (`./_responce_#{$this->Redirect_i}.log`)\n";
#var_dump($this->Cookies);
#var_dump($this->GetCookies());
		}

		$this->ParseHeaders();
		$this->gotCookies_ = $this->GetCookies();
		if ($this->AutoCookies) {
			if ($this->Verbose) {
				file_put_contents("./_responce_#{$this->Redirect_i}.log", 
					"HTTP Page request (source) Cookies: `".trim($this->CookiesToString())."`\r\n", FILE_APPEND);
				echo  " # Server responce log [`./_responce_#{$this->Redirect_i}.log`] updated with `HTTP Page request (source) Cookies`\r\n";

				echo "Trying to set AutoCookies, responce Cookies are: \n";
				var_dump(trim($this->CookiesToString($this->gotCookies_)));
				echo "\n";
			}

			if (!$this->Cookies) $this->Cookies = $this->gotCookies_; #mad_cow O_o
			elseif (is_array($this->Cookies)) $this->Cookies = array_merge($this->Cookies, $this->gotCookies_);
			elseif (is_string($this->Cookies)) $this->Cookies .= $this->CookiesToString($this->gotCookies_);

			if ($this->Verbose) {
				file_put_contents("./_responce_#{$this->Redirect_i}.log", 
					"HTTP Page responce (current) Cookies: `".trim(print_r($this->gotCookies_, true))."`\r\n", FILE_APPEND);
				echo  " # Server total log [`./_responce_#{$this->Redirect_i}.log`] updated with `HTTP Page total (current) Cookies`\r\n";

				file_put_contents("./_responce_#{$this->Redirect_i}.log", 
					"HTTP Page total (result) Cookies: `".trim($this->CookiesToString($this->Cookies))."`\r\n", FILE_APPEND);
				echo  " # Server responce log [`./_responce_#{$this->Redirect_i}.log`] updated with `HTTP Page total (result) Cookies`\r\n";
				echo "\n";

			}
		}


		if ($this->Verbose) {
			file_put_contents("./_responce_#{$this->Redirect_i}.log", 
				"HTTP Page Headers: `".trim(print_r($this->PageHeaders, true))."`\r\n", FILE_APPEND);
			echo  " # Server responce log [`./_responce_#{$this->Redirect_i}.log`] updated with `HTTP Page Headers`\r\n";
		}
/*
			if (!$this->Cookies) $this->Cookies = $this->gotCookies_; #mad_cow O_o
			elseif (is_array($this->Cookies)) $this->Cookies = array_merge($this->Cookies, $this->gotCookies_);
			elseif (is_string($this->Cookies)) $this->Cookies .= $this->CookiesToString($this->gotCookies_);
			if ($this->Verbose) {
				file_put_contents("./_responce_#{$this->Redirect_i}.log", 
					"HTTP Page total Cookies: `".trim(print_r($this->Cookies, true))."`\r\n", FILE_APPEND);
				echo  " # Server total log [`./_responce_#{$this->Redirect_i}.log`] updated with `HTTP Page total (current) Cookies`\r\n";
				#echo "So, resulting Cookies are: \n";
				#var_dump($this->Cookies);
			}
#		}
*/


/*
		
		#curl_setopt($ch, CURLOPT_ENCODING, '');
		#if ($this->gotCookies_) {
		if ($this->Cookies) {
var_dump($this->CookiesToString());
			curl_setopt($this->cURLHandler, CURLOPT_COOKIE, $this->CookiesToString());
			if ($this->Verbose) {
				file_put_contents("./_request_#{$this->Redirect_i}.log", 
					"Cookies: `".trim(print_r($this->CookiesToString()), true)."`\r\n", FILE_APPEND);
var_dump($this->CookiesToString());
				echo  " # Client request log [`./_request_#{$this->Redirect_i}.log`] updated with `Cookies`\r\n";
			}
		}
*/



/*
		#$this->gotCookies__ = $this->Cookies;
		$this->gotCookies_ = $this->GetCookies();
		if ($this->AutoCookies) {
			if ($this->Verbose) {
				file_put_contents("./_responce_#{$this->Redirect_i}.log", 
					#"HTTP Page request Cookies: `".trim(print_r($this->gotCookies__, true))."`\r\n", FILE_APPEND);
					"HTTP Page request Cookies: `".trim(print_r($this->Cookies, true))."`\r\n", FILE_APPEND);
				echo  " # Server responce log [`./_responce_#{$this->Redirect_i}.log`] updated with `HTTP Page request (source) Cookies`\r\n";
				#echo "Trying to set AutoCookies, source Cookies are: \n";
				#var_dump($this->Cookies);

				file_put_contents("./_responce_#{$this->Redirect_i}.log", 
					"HTTP Page responce Cookies: `".trim(print_r($this->gotCookies_, true))."`\r\n", FILE_APPEND);
				echo  " # Server responce log [`./_responce_#{$this->Redirect_i}.log`] updated with `HTTP Page responce (current) Cookies`\r\n";
				#echo "Trying to set AutoCookies, current Cookies are: \n";
				#var_dump($this->gotCookies_);
			}
*/

			
			
/*
			if ($this->Verbose) {
				echo "Current Cookies are: \n";
				var_dump($this->gotCookies_);
			}
			if (!$this->Cookies) $this->Cookies = $this->gotCookies_; #mad_cow O_o
			elseif (is_array($this->Cookies)) $this->Cookies = array_merge($this->Cookies, $this->gotCookies_);
			elseif (is_string($this->Cookies)) $this->Cookies .= $this->CookiesToString($this->gotCookies_);
			if ($this->Verbose) {
				echo "So, resulting Cookies are: \n";
				var_dump($this->Cookies);
			}


			#if (is_array($this->Cookies)) $this->Cookies += $this->GetCookies();
			#$gotCookies = $this->GetCookies();
/*
			$gotCookies = $this->Cookies;
			if ($this->Verbose)
				echo "Above page Cookies: \n".print_r($gotCookies, true)."\n";
*/
#		}




		#$this->GetCookies();
		#$this->gotCookies_ = $this->GetCookies(); # should be declared as `private`
		#}
/*
		if ($this->Verbose && $this->Cookies) {
			file_put_contents("./_responce_#{$this->Redirect_i}.log", 
				"HTTP Page total Cookies: `".trim(print_r($this->Cookies, true))."`\r\n", FILE_APPEND);
			echo  " # Server responce log [`./_responce_#{$this->Redirect_i}.log`] updated with `HTTP Page total Cookies`\r\n";
		}
*/


		if ($this->Verbose && $this->AutoRedirect && isset($this->Headers['location']) && 
			$this->maxRedirects_i < $this->maxRedirects) {
			file_put_contents("./_responce_#{$this->Redirect_i}.log", 
				"HTTP is now redirecting us: ({$this->maxRedirects_i}/{$this->maxRedirects}) to `{$this->Headers['location']}`\r\n", FILE_APPEND);
			echo  " # Server responce log [`./_responce_#{$this->Redirect_i}.log`] updated with `HTTP Redirect`\r\n";
		}


		if ($this->Verbose) {
			file_put_contents("./_responce_#{$this->Redirect_i}.log", 
				"HTTP Page Contents: `".trim(print_r($this->Result, true))."`\r\n", FILE_APPEND);
			echo  " # Server responce log [`./_responce_#{$this->Redirect_i}.log`] updated with `HTTP Page Contents`\r\n";
		}



		#while (preg_match('#^http/1\.[01] [^\r\n]+\r\n\r\n#i', $this->Result)) {
		#if (preg_match('#^http/1\.[01] [^\r\n]+\r\n\r\n#i', $this->Result))
		#	$this->Result = preg_replace("#^http/1\.[01] [^\r\n]+\r\n\r\n#i", '', $this->Result);

/*
		if (!preg_match('#^HTTP/1\.[01] 100#', $this->Result)) {
			if ($this->Verbose)
				echo "Above page Headers: ".print_r($this->PageHeaders, true)."\n";
			if ($this->Verbose) echo "\n\n";
		}
*/
		if (preg_match('#^HTTP/1\.[01] 100#', $this->Result)) {
			if ($this->Verbose)
				echo "Above page Headers: ".print_r($this->PageHeaders, true)."\n";
			if ($this->Verbose) echo "\n\n";
		}
#var_dump("Loc\n");
#var_dump($this->Headers);
#var_dump("{$this->maxRedirects_i} < {$this->maxRedirects}");
		#if ($this->Verbose && $this->AutoRedirect && isset($this->Headers['location']))
		#	echo " Redirect {$this->maxRedirects_i}/{$this->maxRedirects} -> \n";
		if ($this->AutoRedirect && isset($this->Headers['location']) && 
			$this->maxRedirects_i < $this->maxRedirects) {
			$this->maxRedirects_i++;
			#if (substr($this->Headers['location'],0,7) == 'http://') {
			if ($this->Verbose)
				echo "Above page redirected ({$this->maxRedirects_i}/{$this->maxRedirects}) to <{$this->Headers['location']}>\n";

			if (preg_match('#^https?://#i', $this->Headers['location'])) {
				return $this->SendRequest("{$this->Headers['location']}");
			} elseif ($this->Headers['location'][0] == '/') {
				return $this->SendRequest("{$this->Scheme}://{$this->Host}{$this->Headers['location']}");
			} else {
				return $this->SendRequest("{$this->Scheme}://{$this->Host}".
					substr($this->Path,0,strrpos($this->Path,'/')+1)."{$this->Headers['location']}");
			}
		}
		#$this->ResultClean = ltrim(substr($this->Result, strrpos($this->Result, "\r\n\r\n")));
		#$this->ResultClean = preg_match("#Content-Length: [0-9]+\r\n\r\n(.*?)$#si", $this->Result, $clean);
		#preg_match("#[^<>]+:[^<>]+\r\n\r\n(.*?)$#si", $this->Result, $clean);
		#$this->ResultClean = isset($clean[1]) ? $clean[1] : $this->Result;
		$this->ResultClean = ltrim(substr($this->Result, strpos($this->Result, "\r\n\r\n")));
		$this->Referer = false;
		$this->Query = Array();
		$this->Redirect_i++;

		#var_dump($this->Cookies);

		return $this->Result;
	}

	function Get($url) {
		$this->Method = 'get';
		$this->cURLHandler = $this->ConstructQuery();
		curl_setopt($this->cURLHandler, CURLOPT_HTTPGET, true);
		return $this->SendRequest($url, $this->cURLHandler);
	}

	function Post($url, $postfields = false, $force_fields_str = false) {
		$this->Method = 'post';
		if (!$postfields) $postfields = $this->Query;


		if ($force_fields_str) {
			#if (is_array($postfields))
			#	$postfields = http_build_query($postfields);
			#	$postfields = str_replace('+', '%20', $postfields);
				
			if (is_array($postfields)) {
				$postfields_ = '';
				if (is_array($postfields))
					foreach ($postfields as $key => $value) {
						if (isset($value[0]) && $value[0] != '@')
							$value = urlencode($value);
						#var_dump("{$key}={$value}&");
						$postfields_ .= "{$key}={$value}&";
					}
				$postfields_ = rtrim($postfields_, '&');
				$postfields = $postfields_;
			}
				
		}

		#if (is_array($postfields))
		#	$postfields = http_build_query($postfields);
		#	$postfields = str_replace('+', '%20', $postfields);

		# a-la http_build_query()
		/*
			if (is_array($postfields))
				foreach ($postfields as &$field)
					if ($field[0] != '@')
						$field = urlencode(($field));
			$postfields_ = '';
			if (is_array($postfields))
				foreach ($postfields as $key => $value)
					$postfields_ .= "{$key}={$value}&";
			$postfields_ = trim($postfields_, '&');
			$postfields = $postfields_;
		#*#/

		echo "\n\n------------------\n\n";var_dump($postfields);
		if (is_array($postfields))
			foreach ($postfields as &$field)
				if ($field[0] != '@')
					$field = rawurlencode($field);
				#$field = str_replace('+', '%2B', $field);
		echo "\n\n------------------\n\n";var_dump($postfields);
		*/

		$this->cURLHandler = $this->ConstructQuery();
		if ($this->Verbose) {
			echo "Below Post data: ".print_r($postfields, true)."\n";
			file_put_contents("./_request_#{$this->Redirect_i}.log", 
				"Post Vars: `".trim(print_r($postfields, true))."`\r\n", FILE_APPEND);
			echo  " # Client request log [`./_request_#{$this->Redirect_i}.log`] updated with `Post Vars`\r\n";
		}
		curl_setopt($this->cURLHandler, CURLOPT_POST, true);
		curl_setopt($this->cURLHandler, CURLOPT_HTTPGET, false);
		curl_setopt($this->cURLHandler, CURLOPT_POSTFIELDS, $postfields);
		return $this->SendRequest($url, $this->cURLHandler);
	}

	/*
	function GetCookies() {
		#return true;
		preg_match_all('#\sSet-Cookie: *([^ ]+); #si', $this->Result, $cookies, PREG_SET_ORDER);
		foreach ($cookies as $cookie) {
			if ($this->AutoCookies) $this->Cookies .= " {$cookie[1]};";
			#elseif ($this->FlushCookies) $this->Cookies = "{$cookie[1]};";
		}
	}
	*/

}



# If the above methods are not available, but we still need to do the work...

#####     #####     #####     #####     #####     #####     #####     #####     #####     #####     #####     #####     #####
#####     #####     #####     #####     #####     #####     #####     #####     #####     #####     #####     #####     #####

#####     #####     #####     #####     #####     #####

#####     Working with HTTP via sockets...        #####

#####     #####     #####     #####     #####     #####

#####     #####     #####     #####     #####     #####     #####     #####     #####     #####     #####     #####     #####
#####     #####     #####     #####     #####     #####     #####     #####     #####     #####     #####     #####     #####


# Socket based interface
class HTTPSockets extends HTTPClient {
	var $ContentType  = null;
	var $Boundary     = '-----NULL-BYTE----NULL-BYTE----NULL-BYTE';

	var $Silent       = false; # If it true, wouldn't print any messages

	var $ResponseStatus = false; # First line of the response header
	var $ResponseCode = 0; # Response HTTP code

	var $Retries = 3; # Max number of request retries

	function ConstructQuery() {
		$this->MakeAliases();

		if (is_array($this->Query)) {
			if (strtolower($this->ContentType) != 'multipart/form-data') {
				$query_string = '';
				foreach ($this->Query as $key => $value) {
					$query_string .= "$key=$value&";
					//$query_string .= $key.'='.urlencode($value);
				}
				$this->Query = trim($query_string, '&');
			} else {
				$query_string = '';
				foreach ($this->Query as $key => $value) {
					$query_string .= "--{$this->Boundary}\r\n";
					if (!is_array($value)) {
						$query_string .= "Content-Disposition: form-data; name=\"$key\"\r\n\r\n$value\r\n";
					} else {
						/*
						if (!isset($value[1]) && !isset($value[2])) {
							$value[1] = $value[0];
							if (!is_file($value[1])) {
								echo "HTTPQuery: No such file!\n";
								//return false;
							}
							$value[0] = @file_get_contents($value[1]);
							file_put_contents("./!!!{$value[1]}", '');
						}*/
						if (!isset($value[1])) $value[1] = 'somefile';
						if (!isset($value[2])) $value[2] = 'application/octet-stream';
						//if (!isset($value[2])) $value[2] = 'undefined/undefined';
						$query_string .= "Content-Disposition: form-data; name=\"$key\"; ";
						$query_string .= "filename=\"{$value[1]}\"\r\n";
						$query_string .= "Content-Type: {$value[2]}\r\n\r\n{$value[0]}\r\n";
					}
				}
				$query_string .= "--{$this->Boundary}--\r\n";
				$this->Query = $query_string;
			}
		}
	}

	function Filter() {
		$this->Result = null;
		$this->ResultClean = null;

		$this->Method = strtolower($this->Method);
		$this->URI = preg_replace('#^https?://#', '', $this->URI);
		//$this->URI = preg_replace('#[\\]]#', '/', $this->URI);
		//$this->URI = preg_replace('#/{2,}#', '/', $this->URI);
		if (is_string($this->Proxy)) $this->Proxy = explode(':', $this->Proxy);

		$this->ConstructQuery();

		# Erasing data from old request (if current will fail)
		$this->ResponseStatus = false;
		$this->ResponseCode = 0;
	}

	function SendRequest($url = false) {
		if (!$url && !$this->URI) return false;
		if (!$url) $url = $this->URI;
		else $this->URI = $url;
		$this->Filter();

		$url_parts = explode('/', $this->URI);
		$url_parts[0] = explode(':', $url_parts[0]);
		$this->Host = $url_parts[0][0];
		if (isset($url_parts[0][1]) && is_numeric($url_parts[0][1]))
			$this->Port = $url_parts[0][1];
		$this->Path = '';
		for ($i = 1; $i < count($url_parts); $i++)
			$this->Path .= '/'.$url_parts[$i];
		if ($this->Path == '') $this->Path = '/';

		// Creating socket
		$cur_try = 0; // Current try
		$max_tries = $this->Retries; // Max allowed tries
		while (true) {
			if ($this->Proxy) {
				if (is_string($this->Proxy))
					$this->Proxy = explode(':', $this->Proxy);
				$fp = @fsockopen($this->Proxy[0], $this->Proxy[1], $errno, $errstr, $this->Timeout);
			} else {
				$fp = @fsockopen($this->Host, $this->Port, $errno, $errstr, $this->Timeout);
			}
			if ($fp) break; // If everything is ok, going on without stopping

			// Else retrying...
			$cur_try++;
			if (!$this->Silent) echo "Cannot create socket, will now try again ({$cur_try}/{$max_tries})...\n";
			if ($cur_try >= $max_tries) {
				if (!$this->Silent) echo "Too many errors, please check your internet connection!\n";
				return false;
			}
			sleep(0);
		}

		if (($this->Method == 'get') && (substr(0, 1, $this->Query) != '?') && ($this->Query != null))
			$this->Query = '?'.$this->Query;

		$query_length = strlen($this->Query);

		if (!$this->UserAgent)
			$this->UserAgent = 'NULL_byte\'s PHP Browser Mozilla/4.0 (compatible; MSIE 3.0; Windows NT 4.0)';
		if ($this->AutoReferer && !$this->Referer)
			$this->Referer = $this->URI;

		$_cookies = $this->CookiesToString($this->Cookies);

		$PostContentType = 'application/x-www-form-urlencoded';
		if (strtolower($this->ContentType) == 'multipart/form-data')
			$PostContentType = "multipart/form-data; boundary={$this->Boundary}";

		$HTTP_Version = 'HTTP/1.0';
		$out = Array();
		switch ($this->Method) {
			case 'get':
				$out[] = "GET {$this->Path}{$this->Query} {$HTTP_Version}";
				break;
			case 'post':
				$out[] = "POST {$this->Path} {$HTTP_Version}";
				if (isset($this->ContentType))
					$out[] = "Content-Type: {$PostContentType}";
				$out[] = "Content-Length: {$query_length}";
				break;
			default:
				$out[] = strtoupper($this->Method)." {$this->Path}{$this->Query} {$HTTP_Version}";
				break;
		}
		$out[] = "Host: {$this->Host}:{$this->Port}";
		$out[] = "Accept: text/html;q=0.9,application/xhtml+xml;q=0.8,*/*;q=0.5";
		$out[] = "Accept-Language: ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7";
		$out[] = "User-Agent: {$this->UserAgent}";
		if ($this->Cookies) $out[] = "Cookie: {$_cookies}";
		if ($this->Referer) $out[] = "Referer: {$this->Referer}";
		$out = array_merge($out, $this->HTTPHeaders);
		$out[] = "Connection: Close";
		//$out[] = "Connection: Keep-Alive";
		$out = implode("\r\n", $out);

		$out = str_replace("\n", "\r\n", $out);
		$out = preg_replace("#[\n]+#", "\n", $out);
		$out = preg_replace("#[\r]+#", "\r", $out);
		$out .= "\r\n\r\n";
		if ($this->Method == 'post') $out .= $this->Query;

		fwrite($fp, $out);
		$this->Result = '';
		while (!feof($fp))
			$this->Result .= fgets($fp, 1024);
		fclose($fp);
		$this->ResultClean = ltrim(substr($this->Result, strpos($this->Result, "\r\n\r\n")));
		$this->ParseHeaders();

		if ($this->Verbose) {
			echo "----->\n$out\n\n";
			echo "<-----\n{$this->Result}\n\n";
		}
		if ($this->AutoCookies) {
			#if (is_array($this->Cookies)) $this->Cookies += $this->GetCookies();
			if (!$this->Cookies) $this->Cookies = $this->GetCookies();
			elseif (is_array($this->Cookies)) $this->Cookies = array_merge($this->Cookies, $this->GetCookies());
			elseif (is_string($this->Cookies)) $this->Cookies .= $this->GetCookies(true);
		}
		if ($this->AutoRedirect && isset($this->Headers['location']) && 
			$this->maxRedirects_i < $this->maxRedirects) {
			$this->maxRedirects_i++;
			if (substr($this->Headers['location'],0,7) == 'http://') {
				return $this->SendRequest("{$this->Headers['location']}");
			} elseif ($this->Headers['location'][0] == '/') {
				return $this->SendRequest("http://{$this->Host}{$this->Headers['location']}");
			} else {
				return $this->SendRequest("http://{$this->Host}".
					substr($this->Path,0,strrpos($this->Path,'/')+1)."{$this->Headers['location']}");
			}
		}
		$this->Query = NULL;
		$this->Referer = $url;
		$this->ContentType = NULL;
		$this->RawHeaders = Array();
		$this->HTTPHeaders = Array();
		$this->RequestHeaders = Array();

		$this->Redirect_i++;
		return $this->Result;
	}

	function Get($url, $vars = false) {
		if ($vars)
			$this->Query = $vars;
		$this->Method = 'get';
		return $this->SendRequest($url);
	}

	function Post($url, $vars = false, $force_fields_str = false) {
		if ($vars)
			$this->Query = $vars;
		$this->Method = 'post';
		return $this->SendRequest($url);
	}


}


/*
if (!function_exists('class_alias')) {
function class_alias($from, $to) {
	eval("class {$to} extends {$from} {}");
	#eval("abstract class {$to} extends {$from} {}");
}}

if (function_exists('curl_init')) {
	class_alias('HTTPcURL', 'HTTPQuery');
} else {
	class_alias('HTTPSockets', 'HTTPQuery');
}
*/

eval('class HTTPQuery extends '.
	(function_exists('curl_init') ? 'HTTPcURL' : 'HTTPSockets').' {}');



?>