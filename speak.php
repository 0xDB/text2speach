<?php

	# fill default values
	$text = 'EMPTY TEXT';
	$def_lang = 'en';
	$sec_lang = 'ru';
	$cache_path = '../cache'; // relative ./libs folder

	# include dependencies
	require_once dirname(__FILE__).'/libs/WhoIS.php';
	require_once dirname(__FILE__).'/libs/HTTPQuery.php';
	require_once dirname(__FILE__).'/libs/GSpeaker.php';

	# helper function
	function isEng($str) {
		if (!preg_replace('#[a-z0-9\s\!\?\@\#\%\^\$\&\*\(\)\-\+\=\_\/\\\:\;\.\,\"\'\<\>\[\]\{\}]+#si', null, $str))
			return true; else return false;
	}

	# deal with shell (commmand line)
	$lang = $def_lang;
	$lang_arg_length = 2;
	if (isset($_GET['text'])) {
		$text = $_GET['text'];
		$lang = isset($_GET['lang']) ? 
			$_GET['lang'] : (isEng($_GET['text']) ? 'en' : $sec_lang);
	} elseif (isset($_SERVER['argv'][1])) {
		$argv_ = $_SERVER['argv'];
		unset($argv_[0]);
		$text = implode(' ', $argv_);
		if (preg_match("#^[a-z]{{$lang_arg_length}}$#", $_SERVER['argv'][1])) {
			$lang = $_SERVER['argv'][1];
			$text = substr($text, $lang_arg_length+1);
		} else $lang = isEng($text) ? 'en' : $sec_lang;
	}

	# speak up!
	new GSpeaker($text, array('deflang' => $lang, 'cache' => '../cache'));

?>